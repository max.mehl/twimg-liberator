#!/bin/bash
########################################################################
#  Copyright (C) 2017 Max Mehl <mail [at] mehl [dot] mx>
########################################################################
#  
#  This program is free software: you can redistribute it and/or modify 
#  it under the terms of the GNU Affero General Public License as 
#  published by the Free Software Foundation, either version 3 of the 
#  License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful, 
#  but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
#  GNU Affero General Public License for more details.
#  
#  You should have received a copy of the GNU Affero General Public 
#  License along with this program.  If not, see 
#  <http://www.gnu.org/licenses/>.
#  
########################################################################
#  
#  
#  
########################################################################

# Test if config.cfg exists and set needed variables
if [ ! -e config.cfg ]; then echo "Missing config.cfg file. Edit and rename config.cfg.sample"; exit 1; fi
source config.cfg

LINK=$1

if [ "$1" == "" ]; then
  exit 1
fi

CONTENT=$(wget -qO - "$LINK")
TWIMG=$(echo $CONTENT | grep -Pio "https://pbs.twimg.com/media/[a-z0-9-_]{8,}\.(jpg|png|jpeg)" | head -1)

if [ ! -e "$DIR" ]; then
  mkdir $DIR
fi

TWFILE=$(basename $TWIMG)
EXT="${TWFILE##*.}"

DBCHECK=$(grep "^$TWFILE;" $DB)
if [ "$DBCHECK" != "" ]; then   # file has already been downloaded
  echo $DBCHECK | cut -d";" -f2
else                            # file hasn't been downloaded yet
  ok="n"
  while [ "$ok" != "y" ]; do
    RAND=$(openssl rand -hex 3)
    if [ ! -e "$DIR/$RAND.$EXT" ]; then
      ok="y"
    fi
  done

  LCIMG="$DIR/$RAND.$EXT"
  LCLINK="$DOMAIN/$LCIMG"

  SLLINK=$(curl -K .userpass.txt -s "$SLSERV" -d "link=$LCLINK" -A "Mozilla" | grep -P -io "$SLGREP1" | sed -e 's/<[^>]*>//g' | grep -Eo "$SLGREP2")

  echo "$TWFILE;$SLLINK;$LCLINK" >> "$DB"

  wget -O "$LCIMG" "$TWIMG"

  echo "$SLLINK"
fi
